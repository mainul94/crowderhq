frappe.ui.form.on("Purchase Order", {
    refresh: function (frm) {
        frm.add_custom_button(__("Project"), function () {
            frm.trigger('get_items_from_project')
        }, __("Get items from"))
    },
    get_items_from_project: function(frm) {
        if (typeof frm.$project_dailoag === 'undefined') {
            frm.$project_dailoag = new frappe.ui.Dialog({
                title: __("Get Items from Projects"),
                fields: [
                    {
                        fieldtype: "Link",
                        label: __("Project"),
                        fieldname: "project",
                        options: "Project",
                        reqd: true
                    }
                ]
            });
            frm.$project_dailoag.set_primary_action(__("Get"), function (values) {
                if(!values) return;
                console.log(values.project)
                erpnext.utils.map_current_doc({
                    method: "crowderhq.utils.project.make_purchase_order_based_on_project",
                    source_name: values.project,
                    frm:cur_frm
                });
            });
        }
        frm.$project_dailoag.show()
    }
});