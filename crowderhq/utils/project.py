import frappe
from frappe.model.mapper import get_mapped_doc
from six import string_types
from frappe.utils.data import flt


def set_missing_values(source, target_doc):
    # target_doc.run_method("set_missing_values")
    # target_doc.run_method("calculate_taxes_and_totals")
    pass


def update_item(obj, target, source_parent):
    target.conversion_factor = obj.conversion_factor
    target.qty = flt(flt(obj.stock_qty) - flt(obj.ordered_qty))/ target.conversion_factor
    target.stock_qty = (target.qty * target.conversion_factor)


def get_supplier(project_name):
    if frappe.db.exists('Supplier', project_name):
        return frappe.get_doc('Supplier', project_name)
    else:
        doc = frappe.new_doc('Supplier')
        doc.set('supplier_name', project_name)
        sp_group = frappe.get_single('Buying Setting').supplier_group or 'All Supplier Groups'
        doc.set('supplier_group', sp_group)
        doc.save()
        return doc

@frappe.whitelist()
def make_purchase_order_based_on_project(source_name, target_doc=None):
    if target_doc:
        if isinstance(target_doc, string_types):
            import json
            target_doc = frappe.get_doc(json.loads(target_doc))
        target_doc.set("items", [])


    def postprocess(source, target_doc):
        target_doc.supplier = source_name

        set_missing_values(source, target_doc)
        target_doc = get_mapped_doc("Project", source_name, 	{
            "Project": {
                "doctype": "Purchase Order",
                "field_map": [
                    ["name", "project"]
                ]
            },
            "Tasks": {
                "doctype": "Purchase Order Item",
                "field_map": [
                    ["project", "parent"],
                    ["task", "name"],
                    ["item_name", "subject"],
                    ["description", "details"]
                ],
                "postprocess": update_item,
                "condition": lambda doc: doc.parent == source_name
            }
        }, target_doc, postprocess)

        return target_doc