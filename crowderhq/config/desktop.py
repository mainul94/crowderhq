# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Crowder HQ",
			"color": "yellow",
			"icon": "fa fa-pie-chart",
			"type": "module",
			"label": _("Crowder HQ")
		}
	]
